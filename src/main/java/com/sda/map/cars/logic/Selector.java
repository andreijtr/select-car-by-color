package com.sda.map.cars.logic;

import com.sda.map.cars.model.Car;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Selector {

    public Map< String, List<Car> > selectCarsByColor (List<Car> carList) {
        //if color exists in map, add the car in list from map
        //else put that color and create a list and put that car in it
        Map <String, List<Car> > carMap = new HashMap<>();

        for (Car element : carList) {
            String color = element.getColor();

            if (carMap.containsKey(color)) {
                carMap.get(color).add(element);
            } else {
                List<Car> carsWithThisColor = new ArrayList<>();
                carsWithThisColor.add(element);
                carMap.put(color, carsWithThisColor);
            }
        }

        return carMap;
    }
}
