package com.sda.map.cars.printer;

import com.sda.map.cars.model.Car;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class Printer {

    public void printToConsole (Map <String, List<Car>> carMap) {
        //print map after selection by color
        Set<String> keySet = carMap.keySet();

        System.out.println("Cars selected by color:\n");

        for (String element : keySet) {
            System.out.println(">>>>>>>>> " + element + " <<<<<<<<<");
            for (Car car : carMap.get(element)) {
                System.out.println(car);
            }
            System.out.println();
        }
    }
}
