package com.sda.map.cars.runner;

import com.sda.map.cars.logic.Selector;
import com.sda.map.cars.model.Car;
import com.sda.map.cars.printer.Printer;
import com.sda.map.cars.reader.ReaderFromFile;
import com.sda.map.cars.util.FileConstants;

import java.util.List;
import java.util.Map;

public class AppRunner {

    public static void main(String[] args) {

        ReaderFromFile reader = new ReaderFromFile();
        Selector selector = new Selector();
        Printer printer = new Printer();

        List<Car> carList = reader.readAsList(FileConstants.PATH_TO_READ);
        Map<String, List<Car>> resultMap = selector.selectCarsByColor(carList);

        printer.printToConsole(resultMap);
    }
}
