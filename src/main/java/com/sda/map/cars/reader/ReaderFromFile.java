package com.sda.map.cars.reader;

import com.sda.map.cars.model.Car;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ReaderFromFile {

    public List<Car> readAsList (String filePath) {
        //read cars and return a list with cars
        List<Car> carList = new ArrayList<>();

        try {
            File file = new File(filePath);

            FileReader reader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(reader);

            String line = bufferedReader.readLine();

            while (line != null) {
                String[] lineToArray = line.split(" ");

                Car car = new Car();

                car.setModel(lineToArray[0]);
                car.setColor(lineToArray[1]);
                car.setYear(Integer.parseInt(lineToArray[2]));

                carList.add(car);

                line = bufferedReader.readLine();
            }
            bufferedReader.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return carList;
    }
}
